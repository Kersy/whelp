defmodule Whelp.Router do
  use Plug.Router

  @home Application.get_env(:whelp, :home_dir)
  @root Application.get_env(:whelp, :root_dir)
  @wim_home @home <> "/os/"

  plug Plug.Parsers,
     parsers: [
       :multipart,
       :urlencoded,
       {:multipart, length: 8_000_000}, # Increase to 20MB max upload
       {:multipart, read_timeout: :infinity}
     ]

  # plug Plug.Parsers,
  #       parsers: [:urlencoded, :multipart],
  #       body_reader: {Plug.BigFilePlug, :read_body, []}

  plug :match
  plug :dispatch, content: "test data"

  get "/test" do
    body = conn
    |> render("test")

    send_resp(conn,200,body)
  end

  get "/*glob" do
  end

  # get "/hello" do
  #   connection = Web.Controllers.HelloController.call(conn)
  #   body = render(connection, "test")
  #   put_resp_content_type(connection, "text/html")
  #   |> send_resp(200,body)
  # end
  #
  # get "/mount" do
  #   System.cmd("wimlib-imagex",["mount", @home<>"/wims/Win10POS_JUN2018.wim", @wim_home])
  #   conn
  #   |> put_resp_content_type("text/html")
  #   |> send_resp(200, "<html><body><h1>mounted</h1></body></html>")
  # end
  #
  # get "/unmount" do
  #   System.cmd("wimlib-imagex",["unmount", @wim_home])
  #   conn
  #   |> put_resp_content_type("text/html")
  #   |> send_resp(200, "<html><body><h1>unmounted</h1></body></html>")
  # end

  get "/os/*glob" do
    Web.Controllers.FsController.call(conn)
  end

  post "/upload/*glob" do
    upload = conn.params["secret"]
    |> FileTransfer.FileHandler.upload_file

    conn
    |> send_resp(201, "<!DOCTYPE html><html><body><h1>uploaded!</h1></body></html>")
  end

  def multipart(conn,file) do
    #Currently, cowboy reads all the data in the socket,
    #we cannot limit the amount of data we want to read
    #so transfering large files over http isn't helpful
    #will have to hand of to FTP unfortunately.
    case Plug.Conn.read_part_body(conn,[]) do
      {:ok, last_chunk, conn} ->
        IO.binwrite(file,last_chunk)
        conn

      {:more, body_chunk, conn} ->
        IO.binwrite(file,body_chunk)
        multipart(conn,file)

      {:done, conn} ->
        conn
    end
  end

  put "/upload/*glob" do
    conn
    |> send_resp(501, "<!DOCTYPE html><html><body><h1>not available at the moment</h1></body></html>")
  end

  match _ do
    send_resp(conn, 404, "page cannot be found")
  end

  def render(conn,filename) do
    assigns = Enum.to_list conn.assigns
    try do
      EEx.eval_file "/home/matthew/whelp/lib/web/templates/#{filename}.eex", assigns
    rescue
      e in CompileError ->
        details = Enum.reduce(Map.from_struct(e), "",
                  fn {k,v}, acc -> acc <> "<strong>#{k}:</strong> #{v}<br>" end)
        "<h1>Template Compile Error</h1>#{details}"
    end
  end
end
