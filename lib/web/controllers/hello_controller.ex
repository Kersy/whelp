defmodule Web.Controllers.HelloController do
  import Plug.Conn

  def call(conn) do
    conn
    |> assign(:test, "cool test data")
  end

end
