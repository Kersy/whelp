defmodule Web.Controllers.FsController do
  import Plug.Conn

  @home Application.get_env(:whelp, :home_dir)
  @root Application.get_env(:whelp, :root_dir)

  def call(conn) do
    path = Map.get(conn.path_params,"glob")
    |> translate_url
    |> f_type
    |> resp(conn)
  end

  def resp({:directory, path}, conn) do
    IO.inspect path
    {:ok, files} = File.ls(path)
    conn
    |> assign(:files, files)
    |> assign(:url, return_url(Map.get(conn.path_params,"glob")))
    |> render("root")
  end

  def resp({:regular, path}, conn) do
    conn
    |> send_file(200,path)
  end

  def resp({:error, path}, conn) do
    conn
    |> put_resp_content_type("text/html")
    |> send_resp(404,"<!DOCTYPE html><html><body><h1>location not found</h1></body></html>")
  end

  defp translate_url([]), do: @root
  defp translate_url(url), do: @root <> "/" <> Path.join(url)
  defp return_url([]), do: ""
  defp return_url(url), do: "/"<>Path.join(url)

  def f_type(path) do
    {stat, file} = File.stat(path)
    case stat do
      :ok -> {file.type, path}
      error -> {error, path}
    end
  end

  def get_abs_path() do
    :ok
  end

  def render(conn,filename) do
    assigns = Enum.to_list conn.assigns
    try do
      body = EEx.eval_file "/home/matthew/whelp/lib/web/templates/#{filename}.eex", assigns
      conn
      |> put_resp_content_type("text/html")
      |> send_resp(200, body)
    rescue
      e in CompileError ->
        details = Enum.reduce(Map.from_struct(e), "",
                  fn {k,v}, acc -> acc <> "<strong>#{k}:</strong> #{v}<br>" end)
        "<h1>Template Compile Error</h1>#{details}"
        conn
        |> put_resp_content_type("text/html")
        |> send_resp(200,details)
    end
  end

end
