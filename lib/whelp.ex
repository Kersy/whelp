defmodule Whelp do
  use Application

  def start(_type, _args) do

    children = [Plug.Cowboy.child_spec(scheme: :http, plug: Whelp.Router, options: [port: 4040])]

    opts = [strategy: :one_for_one, name: Whelp.Supervisor]
    Supervisor.start_link(children, opts)
  end

end
