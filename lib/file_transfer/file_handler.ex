defmodule FileTransfer.FileHandler do
  import Plug.Conn

  def upload_file(upload) do
    #this is the Plug.Upload struct
    # IO.inspect File.stat(upload.path)
    # IO.inspect Path.join(upload.path,upload.filename)
    # IO.inspect Path.join(File.cwd!,upload.filename)
    File.cp(upload.path,Path.join(File.cwd!,upload.filename))
  end

  def multipart(conn,file) do
    #Currently, cowboy reads all the data in the socket,
    #we cannot limit the amount of data we want to read
    #so transfering large files over http isn't helpful
    #will have to hand of to FTP unfortunately.
    case Plug.Conn.read_part_body(conn,[]) do
      {:ok, last_chunk, conn} ->
        IO.binwrite(file,last_chunk)
        conn

      {:more, body_chunk, conn} ->
        IO.binwrite(file,body_chunk)
        multipart(conn,file)

      {:done, conn} ->
        conn
    end
  end

  # defp parse_headers([]), do: ""
  # defp parse_headers({"content-disposition", data}), do: parse_form_data(data)
  # defp parse_headers([head|tail]), do: parse_headers(head)
  # defp parse_form_data(data), do: [hh | [h|[t|_]]] = String.split(data, ";"); t
end
